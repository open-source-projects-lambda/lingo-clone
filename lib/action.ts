"use server";
import { auth, signIn, signOut } from "@/lib/auth";

//github login action
export const handleGithubLogin = async () => {
  await signIn("github");
};
