import { auth, signIn, signOut } from "@/lib/auth";
import { handleGithubLogin } from "@/lib/action";
import { Button } from "@/components/ui/button";

const LoginPage = () => {
  return (
    <div className="mt-100 px-4 flex flex-col justify-center">
      <div className="mt-100 px-4">
        <form action={handleGithubLogin}>
          <Button variant="ghost">Login with Github</Button>
        </form>
        Login page
      </div>
    </div>
  );
};

export default LoginPage;
